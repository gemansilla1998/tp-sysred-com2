#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>

int clientes = 0;
char estado_barbero[10] = "zzzz";
sem_t sem,sem2;

void* cliente(){
            
	while(1){

		sem_wait(&sem2);
		
		if(clientes < 10){
			
			clientes++;
	
			if(strcmp(estado_barbero,"cortando")==0){
				printf("waiting...\n");
			}
			else if(strcmp(estado_barbero,"zzzz")==0){
				printf("*cof* *cof*\n");
				strcpy(estado_barbero, "despierto");					
			}
		}
		else{
			printf("lleno, mejor busco otra barberia\n");
		}
		
		sem_post(&sem);
   }
}

void* barbero(){

	while(1){
		
		sem_wait(&sem);
		
		if(clientes==0){
			strcpy(estado_barbero, "zzzz");
		}
		else{
			clientes--;
			strcpy(estado_barbero, "cortando");
			printf("*sonidos de tijeras*\n");

		}
		
		sem_post(&sem2);
	}
}


int main(){

	sem_init(&sem,0,1);
	sem_init(&sem2,0,0);
	
	pthread_t p1;
	pthread_t p2;

	pthread_create(&p1, NULL, barbero, NULL);
	pthread_create(&p2, NULL, cliente, NULL);
	pthread_join(p1, NULL);
	pthread_join(p2, NULL);
	
	sem_destroy(&sem);
	sem_destroy(&sem2);

	return 0;
			
}

