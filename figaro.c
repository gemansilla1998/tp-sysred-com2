#include <stdio.h>
#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>
#include <stdlib.h>

sem_t sem1,sem2,sem3,sem4,sem5,sem6;

void* fiiiigaro (void *vueltas){
	
	int vuelta = *(int*)vueltas;
	int cont=0;
	while(cont<vuelta){	
		sem_wait(&sem1);
		printf("Fiiiigaro\n");
		cont++;
		sem_post(&sem2);
	}
}

void* figaro1 (void *vueltas){

	int vuelta = *(int*)vueltas;
	int cont=0;
	
	while(cont<vuelta){
		
		sem_wait(&sem2);
		printf("Figaro\n");
		cont++;
		sem_post(&sem3);
	}
}

void* figaro2 (void *vueltas){

	int vuelta = *(int*)vueltas;
	int cont=0;
	
	while(cont<vuelta){
		
		sem_wait(&sem3);
		printf("Figaro\n");
		cont++;
		sem_post(&sem4);
	}
}

void* figaro3 (void *vueltas){

	int vuelta = *(int*)vueltas;
	int cont=0;
	
	while(cont<vuelta){
		
		sem_wait(&sem4);
		printf("Figaro\n");
		cont++;
		sem_post(&sem5);
	}
}

void* figaro_fi (void *vueltas){

	int vuelta = *(int*)vueltas;
	int cont=0;

	while(cont<vuelta){
		
		sem_wait(&sem5);
		printf("Figaro Fi\n");
		cont++;
		sem_post(&sem6);
	}
}


void* figaro_fa (void *vueltas){
	
	int vuelta = *(int*)vueltas;
	int cont=0;
		
	while(cont<vuelta){
		
		sem_wait(&sem6);
		printf("Figaro Fa\n");
		cont++;
		sem_post(&sem1);
	}
}

int main(){

	sem_init(&sem1, 0, 1);
	sem_init(&sem2, 0, 0);
	sem_init(&sem3, 0, 0);
	sem_init(&sem4, 0, 0);
	sem_init(&sem5, 0, 0);
	sem_init(&sem6, 0, 0);
	
	int vueltas;	

	printf("Ingrese la cantidad de vueltas que desea en la cancion: ");
	scanf("%d", &vueltas);
	
	
	pthread_t p1;
	pthread_t p2;
	pthread_t p3;
	pthread_t p4;
	pthread_t p5;
	pthread_t p6;
	
	pthread_create(&p1, NULL,fiiiigaro,(void *)&vueltas);
	pthread_create(&p2, NULL,figaro1, (void *)&vueltas);
	pthread_create(&p3, NULL,figaro2, (void *)&vueltas);
	pthread_create(&p4, NULL,figaro3, (void *)&vueltas);
	pthread_create(&p5,NULL,figaro_fi, (void *)&vueltas);
	pthread_create(&p6,NULL,figaro_fa, (void *)&vueltas);

	pthread_join(p1,NULL);
	pthread_join(p2,NULL);
	pthread_join(p3,NULL);
	pthread_join(p4,NULL);
	pthread_join(p5,NULL);
	pthread_join(p6,NULL);

	sem_destroy(&sem1);
	sem_destroy(&sem2);
	sem_destroy(&sem3);
	sem_destroy(&sem4);
	sem_destroy(&sem5);
	sem_destroy(&sem6);
	
	return 0;
}


