#include<stdio.h>
#include<stdlib.h>

void suma(int num1, int num2, int *resultado){
	int sum = num1 + num2;
	*resultado = sum;
}
void resta(int num1, int num2, int *resultado){
	int res = num1 - num2;
	*resultado = res;
}
void mult(int num1, int num2, int *resultado){
	int mul = num1 * num2;
	*resultado = mul;
}
void divi(int num1, int num2, int *resultado){
	int div;	
	
	if(num2 != 0){
		div = num1 / num2;
	}
	else{
		div = num2 / num1;
	}
	*resultado = div;
}
int main(){
	int resultado;
	int valorUsuario;
	int valorUsuario2;

	printf("Ingrese un numero entero: ");
	scanf("%d", &valorUsuario);
	printf("Ingrese otro numero entero distinto de 0: ");
	scanf("%d", &valorUsuario2);


		suma(valorUsuario, valorUsuario2, &resultado);
		printf("\nEl Resultado de la suma es: %d \n", resultado);
		resta(valorUsuario, valorUsuario2, &resultado);
		printf("El Resultado de la resta es: %d \n", resultado);
		mult(valorUsuario, valorUsuario2, &resultado);
		printf("El Resultado de la multiplicacion es: %d \n", resultado);
		divi(valorUsuario, valorUsuario2, &resultado);
		printf("El Resultado de la division es: %d \n", resultado);	

	return 0;
}
